# Neura

![neura logo, and i can't make relative links work](https://gitlab.com/antithesislabs/neura/-/raw/master/assets/neura-logo.png)

## What is Neura?

Neura is a highly printable 3D printer design which makes use of printed and standardized parts to create a flexible and inexpensive platform for getting started in additive manufacturing.

Neura's design philosophy is based around the core concept of 'availability'. Replacement parts should be easily printable where possible, or readily available off-the-shelf otherwise. The need for specialized or esoteric knowledge should be an asset but not a requirement when operating this machine. In addition, Antithesis Labs is committed to supporting and working with open source development throughout Neura's product lifetime.

* 95x95x95 mm build volume
* E3D Lite6 hotend
* 2GT belt-driven XY axes; M5 threaded rod Z
* 608RS/ZZ bearing idlers and wheels
* Custom-tooled aluminium Antithesis Labs heated beds
* Printed rails with 8mm steel rod reinforcement

Neura’s build volume makes printing accessible at a lower cost to the end user without compromising commitments to high quality components and open source standards. In addition, extensibility is a vital part of Neura’s build philosophy. Potential upgrades are built into the design and made simple to implement. Hardware-wise, Antithesis Labs’ PEI-coated aluminum bed ships with high-temperature capability out of the box, easily reaching the 100 C needed for ABS and other exotic filaments. Neura's E3D Lite6 hotend is fully compatible with the V6 ecosystem (Revo support still in development). The aluminium extruder mounts via a standardized NEMA 17 pattern and is upgradeable with geared or any number of open-source alternatives.

On the firmware side, Neura is designed to run Marlin 2.0 on a RAMPS 1.6 + Atmega2560 sandwich. This configuration is well documented and allows for an easier transition towards enabling advanced features or upgrading existing firmware.

---

*To follow along with Neura’s development and see the progress of this project, follow developer @xakh (James Lynch) on Twitter or visit antithesislabs.com.*

Antithesis Labs LLC is a 3D printing development, manufacturing, and distribution company based in San Antonio, TX. 


